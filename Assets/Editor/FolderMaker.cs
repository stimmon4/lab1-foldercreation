﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

public class FolderMaker : MonoBehaviour
/**
* @author Seth Timmons
* Create folders using a menu button
*/
{
    [MenuItem("Tool Creation/Create Folder")]
    public static void CreateFolder()

    {
        //Create the dynamic assets folder and the text file explaining what's contained in it
        AssetDatabase.CreateFolder("Assets", "Dynamic Assets");
        File.WriteAllText(Application.dataPath + "/Dynamic Assets/folderStructure.txt", "This folder is for storing dynamic assets!");
        //Create the resources folder under dynamic assets
        AssetDatabase.CreateFolder("Assets/Dynamic Assets", "Resources");
        //Create the animations folder and all its subfolders under dynamic assets
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animations");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Animations", "Sources");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animation Controllers");
        //Create the effects folder under dynamic assets
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Effects");
        //Create the models folder and all its subfolders under dynamic assets
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Models");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Character");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Environment");
        //Create the prefabs folder and its subfolder under dynamic assets
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Prefabs");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Prefabs", "Common");
        //Create the sounds folder and all its subfolders under dynamic assets
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Sounds");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "Music");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/Music", "Common");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "SFX");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/SFX", "Common");
        //Create the textures folder and its subfolder under dynamic assets
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Textures");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Textures", "Common");
        //Create the extensions folder and the text file explaining what's in it
        AssetDatabase.CreateFolder("Assets", "Extensions");
        File.WriteAllText(Application.dataPath + "/Extensions/folderStructure.txt", "This folder is for storing third party assets!");
        //Create the gizmos folder and the text file explaining what's in it
        AssetDatabase.CreateFolder("Assets", "Gizmos");
        File.WriteAllText(Application.dataPath + "/Gizmos/folderStructure.txt", "This folder is for storing gizmo scripts!");
        //Create the plugins folder and the text file explaining what's in it
        AssetDatabase.CreateFolder("Assets", "Plugins");
        File.WriteAllText(Application.dataPath + "/Plugins/folderStructure.txt", "This folder is for storing plugin scripts!");
        //Create the scripts folder, its subfolder, and the text file explaining what's in it
        AssetDatabase.CreateFolder("Assets", "Scripts");
        File.WriteAllText(Application.dataPath + "/Scripts/folderStructure.txt", "This folder is for storing all other scripts!");
        AssetDatabase.CreateFolder("Assets/Scripts", "Common");
        //Create the shaders folder and the text file explaining what it does
        AssetDatabase.CreateFolder("Assets", "Shaders");
        File.WriteAllText(Application.dataPath + "/Shaders/folderStructure.txt", "This folder is for storing shader scripts!");
        //Create the static assets folder and the text file explaining what it does
        AssetDatabase.CreateFolder("Assets", "Static Assets");
        File.WriteAllText(Application.dataPath + "/Static Assets/folderStructure.txt", "This folder is for storing static assets!");
        //Create the animations folder and its subfolder under static assets
        AssetDatabase.CreateFolder("Assets/Static Assets", "Animations");
        AssetDatabase.CreateFolder("Assets/Static Assets/Animations", "Sources");
        //Create the animation controllers folder under static assets
        AssetDatabase.CreateFolder("Assets/Static Assets", "Animation Controllers");
        //Create the effects folder under static assets
        AssetDatabase.CreateFolder("Assets/Static Assets", "Effects");
        //Create the models folder and its subfolders under static assets 
        AssetDatabase.CreateFolder("Assets/Static Assets", "Models");
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Character");
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Environment");
        //Create the prefabs folder and its subfolder under static assets
        AssetDatabase.CreateFolder("Assets/Static Assets", "Prefabs");
        AssetDatabase.CreateFolder("Assets/Static Assets/Prefabs", "Common");
        //Create the scenes folder under static assets
        AssetDatabase.CreateFolder("Assets/Static Assets", "Scenes");
        //Create the sounds folder and its subfolders under static assets
        AssetDatabase.CreateFolder("Assets/Static Assets", "Sounds");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "Music");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/Music", "Common");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "SFX");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/SFX", "Common");
        //Create the textures folder and its subfolder under static assets
        AssetDatabase.CreateFolder("Assets/Static Assets", "Textures");
        AssetDatabase.CreateFolder("Assets/Static Assets/Textures", "Common");
        //Create the testing folder
        AssetDatabase.CreateFolder("Assets", "Testing");

        AssetDatabase.Refresh();





    }

}
